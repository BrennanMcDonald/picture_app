package io.brennanmcdonald.nonameapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import io.brennanmcdonald.nonameapp.Helpers.AmazonHelper;
import io.brennanmcdonald.nonameapp.Models.PictureModel;
import io.brennanmcdonald.nonameapp.R;

/**
 * Created by Brennan on 7/6/2015.
 */
public class PictureGridAdapter extends ArrayAdapter<PictureModel> {

    private final Context context;
    private final int resourceID;
    Picasso p;

    public PictureGridAdapter(Context context, int resource, ArrayList<PictureModel> bah) {
        super(context, resource, bah);

        this.context = context;
        this.resourceID = resource;
        p = new Picasso.Builder(context)
                .memoryCache(new LruCache(24000))
                .build();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(resourceID, parent, false);
        ImageView imgView = ((ImageView) rowView.findViewById(R.id.imageView));
        final ProgressBar pBar = (ProgressBar) rowView.findViewById(R.id.pBar);

        TextView description = ((TextView) rowView.findViewById(R.id.descriptionText));
        description.setText(getItem(position).Location);

        p.with(context)
                .load(getItem(position).getURI(AmazonHelper.BUCKETNAME))
                .resize(500,500)
                .centerCrop()
                .into(imgView, new Callback() {
                    @Override
                    public void onSuccess() {
                        pBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });

        final PictureModel s = getItem(position);


        return rowView;
    }
}