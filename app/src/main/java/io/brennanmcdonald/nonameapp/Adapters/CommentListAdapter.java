package io.brennanmcdonald.nonameapp.Adapters;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.brennanmcdonald.nonameapp.Models.CommentModel;
import io.brennanmcdonald.nonameapp.R;

/**
 * Created by Brennan on 7/6/2015.
 */
public class CommentListAdapter extends ArrayAdapter<CommentModel> {
    List<CommentModel> elements;
    int resourceID;
    Context myContext;
    public CommentListAdapter(Context context, int resource, ArrayList<CommentModel> inArray) {
        super(context, resource, inArray);
        resourceID = resource;
        elements = inArray;
        myContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View commentView = inflater.inflate(resourceID, parent, false);

        TextView text = ((TextView)commentView.findViewById(R.id.commentMessage));
        TextView date = ((TextView)commentView.findViewById(R.id.commentDate));
        if (getItem(position).Text != null)
            text.setText(getItem(position).Text);
        if (getItem(position).Timestamp != null)
            date.setText(DateUtils.getRelativeTimeSpanString(myContext, Long.parseLong(String.valueOf(getItem(position).Timestamp))));

        return commentView;
    }
}
