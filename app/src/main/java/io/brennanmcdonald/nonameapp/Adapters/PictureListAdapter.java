package io.brennanmcdonald.nonameapp.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import io.brennanmcdonald.nonameapp.Helpers.AmazonHelper;
import io.brennanmcdonald.nonameapp.Models.PictureModel;
import io.brennanmcdonald.nonameapp.R;

/**
 * Created by Brennan on 7/6/2015.
*/
public class PictureListAdapter extends ArrayAdapter<PictureModel> {

    private final Context context;
    private final int resourceID;
    Picasso p;

    public PictureListAdapter(Context context, int resource, ArrayList<PictureModel> bah) {
        super(context, resource, bah);

        this.context = context;
        this.resourceID = resource;

        try {
            p = new Picasso.Builder(context)
                    .memoryCache(new LruCache(24000))
                    .build();
        } catch(Exception e) {
            Log.e("PictureListAdapter", e.getMessage());
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            // TODO: Refractor this, make it cleaner
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View rowView = inflater.inflate(resourceID, parent, false);

            TextView description = ((TextView) rowView.findViewById(R.id.descriptionText));
            description.setText(getItem(position).Location);

            ImageView imgView = ((ImageView) rowView.findViewById(R.id.imageView));
            final ProgressBar pBar = (ProgressBar) rowView.findViewById(R.id.pBar);

            p.with(context)
                    .load(getItem(position).getURI(AmazonHelper.BUCKETNAME))
                    .resize(1000, 1000)
                    .centerCrop()
                    .into(imgView, new Callback() {
                        @Override
                        public void onSuccess() {
                            pBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });

            final PictureModel s = getItem(position);
            return rowView;
        } catch(Exception e) {
            Log.e("PLA.getView", e.getMessage());
            return null;
        }
    }
}
