package io.brennanmcdonald.nonameapp.Activites;

import java.net.URI;
import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;

import io.brennanmcdonald.nonameapp.Adapters.PictureGridAdapter;
import io.brennanmcdonald.nonameapp.Adapters.PictureListAdapter;
import io.brennanmcdonald.nonameapp.Helpers.AsyncHelper;
import io.brennanmcdonald.nonameapp.Models.PictureModel;
import io.brennanmcdonald.nonameapp.R;


public class HomeActivity extends ActionBarActivity implements
        ActionBar.TabListener {

    SectionsPagerAdapter mSectionsPagerAdapter;

    static ViewPager mViewPager;
    static ListView imageList;

    final static public String BUCKETNAME = "picture-api-bucket";
    final static public String PREFERENCEFILE = "pictureSettings";

    static public String StoredPhonenumber;
    static public String StoredUserGUID;
    static public SharedPreferences prefs;
    static public boolean loaded;
    static public String longa;
    static public String lata;
    static public LocationManager mLocationManager;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1; // 1 minute

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            final ActionBar actionBar = getSupportActionBar();
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

            mViewPager = (ViewPager) findViewById(R.id.pager);
            mViewPager.setAdapter(mSectionsPagerAdapter);

            mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, mLocationListener);

            imageList = (ListView) findViewById(R.id.primaryImageList);

            mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    actionBar.setSelectedNavigationItem(position);
                }
            });

            for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
                actionBar.addTab(
                        actionBar.newTab()
                                .setText(mSectionsPagerAdapter.getPageTitle(i))
                                .setTabListener(this));
            }

            mViewPager.setCurrentItem(1);

            prefs = getSharedPreferences(PREFERENCEFILE, 0);

            try {
                if (prefs.contains("phonenumber") && prefs.contains("userguid")) {
                    StoredPhonenumber = prefs.getString("phonenumber", "");
                    StoredUserGUID = prefs.getString("userguid", "");
                } else {
                    SharedPreferences.Editor editor = prefs.edit();
                    TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                    String[] params = new String[2];
                    params[0] = tMgr.getLine1Number();
                    params[1] = UUID.randomUUID().toString();

                    StoredPhonenumber = params[0];
                    StoredUserGUID = params[1];

                    editor.putString("phonenumber", params[0]);
                    editor.putString("userguid", params[1]);

                    editor.commit();

                    AsyncTask<String, Void, String> userPoster = new AsyncHelper.addUser().execute(params);
                }
            } catch (Exception e) {
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.menu_main, menu);
            ListView listView = (ListView) findViewById(R.id.primaryImageList);
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.attachToListView(listView);

            return true;
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 1:
                    return ImageListView.newInstance(0);
                case 0:
                    return ProfileFragment.newInstance(1);
                case 2:
                    return SettingsFragment.newInstance(2);
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 1:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 0:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }

    public static class ImageListView extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public static ListView varImageView;
        public static ArrayList<PictureModel> imageList = new ArrayList<PictureModel>();

        public int index;
        public int position;


        public static ImageListView newInstance(int sectionNumber) {
            try {
                ImageListView fragment = new ImageListView();
                Bundle args = new Bundle();
                args.putInt(ARG_SECTION_NUMBER, sectionNumber);
                fragment.setArguments(args);
                return fragment;
            } catch (Exception e) {
                return null;
            }
        }

        public ImageListView() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            Toast.makeText(getActivity().getBaseContext(), "onCreateView", Toast.LENGTH_SHORT).show();
            super.onCreateView(inflater, container, savedInstanceState);
            View rootView = inflater.inflate(R.layout.fragment_listview, container, false);

            final ListView listImageView = (ListView) rootView.findViewById(R.id.primaryImageList);


            try {
                varImageView = listImageView;

                String[] s = new String[1];
                s[0] = "http://ec2-54-148-117-166.us-west-2.compute.amazonaws.com/api/picture/";

                //TODO: De-break stuff
                final AsyncTask<String, Void, PictureModel[]> AST = new AsyncHelper.RetrievePictures(getActivity().getBaseContext(), rootView).execute(s);
                PictureModel[] varData = AST.get();

                if (varData == null) {
                    varData = new PictureModel[0];
                }
                final PictureModel[] data = varData;
                varData = data;
                listImageView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent commentIntent = new Intent(parent.getContext(), CommentActivity.class);
                        commentIntent.putExtra("image", data[position].getURI(BUCKETNAME));
                        commentIntent.putExtra("userGUID", StoredUserGUID);
                        commentIntent.putExtra("Name", data[position].Name);
                        startActivity(commentIntent);
                    }
                });


                ((ListView) rootView.findViewById(R.id.primaryImageList)).setDivider(null);
                ((ListView) rootView.findViewById(R.id.primaryImageList)).setDividerHeight(0);

                imageList.clear();

                if (!loaded) {

                    for (int i = 0; i < varData.length; i++) {
                        imageList.add(varData[i]);
                    }

                PictureListAdapter adapter = new PictureListAdapter(rootView.getContext(), R.layout.row, imageList);
                listImageView.setAdapter(adapter);
                }
                loaded = true;
                return rootView;
            } catch (Exception e) {
                Toast.makeText(getActivity().getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                return null;
            }

        }

        @Override
        public void onPause() {
            try {
                index = varImageView.getFirstVisiblePosition();
                View v = varImageView.getChildAt(0);
                position = (v == null) ? 0 : (v.getTop() - varImageView.getPaddingTop());
                super.onPause();
            } catch (Exception e) {
                Toast.makeText(getActivity().getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                super.onPause();
            }
        }

        @Override
        public void onResume() {
            try {
                refreshList(getView());
                ((ListView) getView().findViewById(R.id.primaryImageList)).setSelection(position);
                ((ListView) getView().findViewById(R.id.primaryImageList)).setSelectionFromTop(index, position);
                super.onResume();
            } catch (Exception e) {
                Toast.makeText(getActivity().getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                super.onResume();
            }
        }

        public void refreshList(View v) {
            ((ListView) v.findViewById(R.id.primaryImageList)).setDivider(null);
            ((ListView) v.findViewById(R.id.primaryImageList)).setDividerHeight(0);

            ListView imageView = (ListView) v.findViewById(R.id.primaryImageList);
            try {
                imageList.clear();
                String[] s = new String[1];
                s[0] = "http://ec2-54-148-117-166.us-west-2.compute.amazonaws.com/api/picture/";
                //TODO: De-break stuff
                AsyncTask<String, Void, PictureModel[]> AST = new AsyncHelper.RetrievePictures(getActivity().getBaseContext(), v).execute(s);
                PictureModel[] data = AST.get();

                for (int i = 0; i < data.length; i++) {
                    imageList.add(data[i]);
                }

                PictureListAdapter adapter = new PictureListAdapter(v.getContext(), R.layout.row, imageList);
                imageView.setAdapter(adapter);

            } catch (Exception e) {
                Toast.makeText(getActivity().getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    public static class ProfileFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public static ProfileFragment newInstance(int sectionNumber) {
            try {
                ProfileFragment fragment = new ProfileFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_SECTION_NUMBER, sectionNumber);
                fragment.setArguments(args);
                return fragment;
            } catch (Exception e) {
                return null;
            }
        }

        public ProfileFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            try {
                super.onCreateView(inflater, container, savedInstanceState);

                View rootView = inflater.inflate(R.layout.profile_fragment, container, false);

                TextView phoneTextView = (TextView) rootView.findViewById(R.id.profile_phone);
                phoneTextView.setText(StoredPhonenumber);
                TextView guidTextView = (TextView) rootView.findViewById(R.id.profile_guid);
                guidTextView.setText(StoredUserGUID);

                return rootView;
            } catch (Exception e) {
                Toast.makeText(getActivity().getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                return null;
            }
        }

        @Override
        public void onPause() {
            super.onPause();
        }

        @Override
        public void onResume() {
            super.onResume();

        }


    }

    public static class SettingsFragment extends Fragment {

        public static ArrayList<PictureModel> imageList = new ArrayList<PictureModel>();
        public int position;

        private static final String ARG_SECTION_NUMBER = "section_number";

        public static SettingsFragment newInstance(int sectionNumber) {
            try {
                SettingsFragment fragment = new SettingsFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_SECTION_NUMBER, sectionNumber);
                fragment.setArguments(args);
                return fragment;
            } catch (Exception e) {
                return null;
            }
        }

        public SettingsFragment() {

        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            try {
                super.onCreateView(inflater, container, savedInstanceState);
                View rootView = inflater.inflate(R.layout.grid_fragment, container, false);

                final GridView imageView = (GridView) rootView.findViewById(R.id.imageGridView);
                PictureModel[] varData = null;

                String[] s = new String[1];
                s[0] = "http://ec2-54-148-117-166.us-west-2.compute.amazonaws.com/api/picture/";


                final AsyncTask<String, Void, PictureModel[]> AST = new AsyncHelper.RetrievePictures(getActivity().getBaseContext(), rootView).execute(s);
                final PictureModel[] data = AST.get();

                varData = data;
                ((GridView) rootView.findViewById(R.id.imageGridView))
                        .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent commentIntent = new Intent(parent.getContext(), CommentActivity.class);
                                commentIntent.putExtra("image", data[position].getURI(BUCKETNAME));
                                commentIntent.putExtra("userGUID", StoredUserGUID);
                                commentIntent.putExtra("Name", data[position].Name);
                                startActivity(commentIntent);
                            }
                        });


                imageList.clear();
                //TODO: De-break stuff


                for (int i = 0; i < varData.length; i++) {
                    imageList.add(varData[i]);
                }

                PictureGridAdapter adapter = new PictureGridAdapter(rootView.getContext(), R.layout.grid_row, imageList);
                imageView.setAdapter(adapter);


                return rootView;
            } catch (Exception e) {
                Toast.makeText(getActivity().getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                return null;
            }
        }
    }

    public void startPictureView(View v) {
        try {
            Intent cameraIntent = new Intent(this, PostPictureActivity.class);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, new URI("file:///sdcard/picture.png"));
            cameraIntent.putExtra("LATA", lata);
            cameraIntent.putExtra("LONGA", longa);
            startActivity(cameraIntent);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            longa = String.valueOf(location.getLongitude());
            lata = String.valueOf(location.getLatitude());
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };
}
