package io.brennanmcdonald.nonameapp.Activites;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.brennanmcdonald.nonameapp.Adapters.CommentListAdapter;
import io.brennanmcdonald.nonameapp.Helpers.AsyncHelper;
import io.brennanmcdonald.nonameapp.Models.CommentModel;
import io.brennanmcdonald.nonameapp.R;


public class CommentActivity extends ActionBarActivity {

    public int index;
    public int position;
    public String phoneNumber;
    public String mName;

    final static public int COMMENT_INTENT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent intent = getIntent();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        Picasso.with(this)
                .load(intent.getStringExtra("image"))
                .resize(500, 500)
                .centerCrop()
                .into((ImageView) findViewById(R.id.imageInFocus));

        phoneNumber = intent.getStringExtra("userGUID");
        mName = intent.getStringExtra("Name");

        AsyncTask<String, Void, ArrayList<CommentModel>> getComments = new AsyncHelper
                .getComments()
                .execute(new String[]{mName});
        ArrayList<CommentModel> listItems = new ArrayList<CommentModel>();

        try {
            listItems = getComments.get();
        } catch (Exception e) {
            listItems = null;
            Log.e("CommentActivity", e.getMessage());
        }

        if (listItems == null || listItems.isEmpty()) {
            listItems.add(new CommentModel());
        }

        ((ListView) findViewById(R.id.commentList)).setAdapter(new CommentListAdapter(this, R.layout.commentrow, listItems));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_comment, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        i.putExtra("scrollPosition", position);
        i.putExtra("scrollIndex", index);
        setResult(COMMENT_INTENT, i);
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void postComment(View v) {

        String message =
                ((TextView) findViewById(R.id.commentMessageText))
                        .getText()
                        .toString();

        if (message.isEmpty()){
            Toast.makeText(this, "Please enter a comment", Toast.LENGTH_SHORT).show();
        }

        CommentModel commentToPost = new CommentModel();
        commentToPost.Text = message;
        commentToPost.UserGUID = phoneNumber;
        commentToPost.ParentName = mName;

        try {
            AsyncTask<CommentModel, Void, Boolean> postTask = new AsyncHelper.addComment().execute(commentToPost);
            if (postTask.get()) {
                Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Fail", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
