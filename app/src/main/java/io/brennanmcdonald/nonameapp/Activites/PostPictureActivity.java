package io.brennanmcdonald.nonameapp.Activites;

/**
 * @author Jose Davis Nidhin
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.UUID;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import io.brennanmcdonald.nonameapp.Helpers.AsyncHelper;
import io.brennanmcdonald.nonameapp.Models.UploadModel;
import io.brennanmcdonald.nonameapp.R;

public class PostPictureActivity extends Activity {
	private static final String TAG = "CamTestActivity";
	private Preview preview;
	private Button buttonClick;
	private Camera camera;
	private Activity act;
	private Context ctx;
	private Camera.Parameters parameters;
	private String lata;
	private String longa;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ctx = this;
		act = this;
		lata = getIntent().getStringExtra("LATA");
		longa = getIntent().getStringExtra("LONGA");
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_post_picture);

		preview = new Preview(this, (SurfaceView)findViewById(R.id.cameraPreviewSurface));
		preview.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		((FrameLayout) findViewById(R.id.CameraPreviewFrame)).addView(preview);
		preview.setKeepScreenOn(true);

	}

	@Override
	protected void onResume() {
		super.onResume();
		int numCams = Camera.getNumberOfCameras();
		if(numCams > 0){
			try{
				camera = Camera.open(0);

				parameters = camera.getParameters();
				parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
				parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
				parameters.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
				parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
				parameters.setExposureCompensation(0);
				parameters.setPictureFormat(ImageFormat.JPEG);
				parameters.setRotation(90);
				parameters.setJpegQuality(25);

				List<Camera.Size> sizes = parameters.getSupportedPictureSizes();
				Camera.Size size = sizes.get(0);
				for(int i=0;i<sizes.size();i++)
				{
					if(sizes.get(i).width > size.width)
						size = sizes.get(i);
				}

				parameters.setPictureSize(size.width, size.height);
				camera.setParameters(parameters);
				camera.setDisplayOrientation(90);
				camera.setParameters(parameters);
				camera.startPreview();

				preview.setCamera(camera);
			} catch (RuntimeException ex){
				Toast.makeText(ctx, "Camera Not Found", Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	protected void onPause() {
		if(camera != null) {
			camera.stopPreview();
			preview.setCamera(null);
			camera.release();
			camera = null;
		}
		super.onPause();
	}

	private void resetCam() {
		camera.startPreview();
		preview.setCamera(camera);
	}

	private void refreshGallery(File file) {
		Intent mediaScanIntent = new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		mediaScanIntent.setData(Uri.fromFile(file));
		sendBroadcast(mediaScanIntent);
	}
	private class SaveImageTask extends AsyncTask<byte[], Void, Void> {

		@Override
		protected Void doInBackground(byte[]... data) {
			FileOutputStream outStream = null;

			// Write to SD Card
			try {
				File sdCard = Environment.getExternalStorageDirectory();
				File dir = new File (sdCard.getAbsolutePath() + "/camtest");
				dir.mkdirs();

				String fileName = String.format("%d.jpg", System.currentTimeMillis());
				File outFile = new File(dir, fileName);

				outStream = new FileOutputStream(outFile);
				outStream.write(data[0]);
				outStream.flush();
				outStream.close();

				Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length + " to " + outFile.getAbsolutePath());

				refreshGallery(outFile);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
			}
			return null;
		}

	}

	public void TakePicture(View v){
		camera.takePicture(null, null, mPicture);

		((TextView)findViewById(R.id.CommentText)).setVisibility(View.VISIBLE);
		((ImageButton)findViewById(R.id.ContinueButton)).setVisibility(View.VISIBLE);
		((ImageButton)findViewById(R.id.PictureButton)).setVisibility(View.INVISIBLE);
		((ImageView)findViewById(R.id.PostedImageView)).setVisibility(View.VISIBLE);

		// TODO: shit
	}

	public void PostPicture(View v){
		UploadModel c = new UploadModel();
		try {
			File pictureFile = new File(new URI("file://" + Environment.getExternalStorageDirectory().getPath()+ "/picture.png"));

			c.image = BitmapFactory.decodeFile(pictureFile.getAbsolutePath());
			c.context = this;
			c.filename = UUID.randomUUID().toString() + ".png";
			c.description = ((TextView) findViewById(R.id.CommentText)).getText().toString();
			c.latitude = lata;
			c.longitude = longa;

			AsyncTask<UploadModel, Void, Boolean> asyncPostTask = new AsyncHelper.addPictureToDatabase().execute(c);
		} catch(Exception e){
			Log.d(getClass().getSimpleName(), e.getMessage());
		} finally {
			finish();
		}
	}

	private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			File pictureFile;
			try {
				pictureFile = new File(Environment.getExternalStorageDirectory().getPath()+ "/picture.png");
				if (pictureFile == null) {
					Log.d(getClass().getSimpleName(), "Error creating media files");
					return;
				}
			} catch (Exception e){
				Log.d(getClass().getSimpleName(), e.getMessage());
				pictureFile = null;
			}

			try {
				FileOutputStream fs = new FileOutputStream(pictureFile);
				fs.write(data);
				fs.close();
			} catch (Exception e){
				Log.d(getClass().getSimpleName(), e.getMessage());
			}
		}
	};
}
