package io.brennanmcdonald.nonameapp.Models;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import io.brennanmcdonald.nonameapp.Helpers.AsyncHelper;

/**
 * Created by Brennan on 7/6/2015.
 */
public class PictureModel {

    //TODO: Model this after the database Model
    public Integer ID;
    public String Name;
    public String Filename;
    public String Width;
    public String Height;
    public String UserGuid;
    public String Location;

    private Bitmap bmp;

    public AsyncTask<String, Void, Bitmap> getBitmapAsync;

    public PictureModel(String iID, String iName, String iFilename, String iWidth, String iHeight, String iUserGuid, String iLocation) {
        Filename = iFilename;
        Name = iName;
        Width = iWidth;
        Height = iHeight;
        UserGuid = iUserGuid;
        Location = iLocation;
        ID = Integer.parseInt(iID);
    }

    public String getURI(String BucketName) {
        return "https://s3-us-west-2.amazonaws.com/" + BucketName + "/" + Filename;
    }

    public Bitmap getBitmap() {
        try {
            if (bmp == null) {

                if (ID == -2) {
                    return Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
                } else {
                    getBitmapAsync = new AsyncHelper.getPicture().execute(new String[]{Filename});
                    bmp = getBitmapAsync.get();
                    while (bmp == null) {
                    }
                    return bmp;
                }

            } else {
                return bmp;
            }
        } catch (Exception e) {
            return Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
        }
    }
}
