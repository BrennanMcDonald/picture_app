package io.brennanmcdonald.nonameapp.Models;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * Created by Brennan on 7/6/2015.
 */
public class UploadModel {
    public Context context;
    public Bitmap image;
    public String filename;
    public String description;
    public String latitude;
    public String longitude;
    public String URI() {
        return "api/picture/?filename=" + filename + "&description=" + description + "&lata=" + latitude + "&longi=" + longitude;
    }
}
