package io.brennanmcdonald.nonameapp.Models;

/**
 * Created by Brennan on 7/6/2015.
 */
public class CommentModel {
    int ID;
    public String ParentName;
    public String Text;
    public String UserGUID;
    public String CommentGUID;
    public Long Timestamp;

    public CommentModel(String iID, String iParentName, String iText, String iUserGUID, String iCommentGUID, String iTimestamp){
        ID = Integer.parseInt(iID);
        ParentName = iParentName;
        Text = iText;
        UserGUID = iUserGUID;
        CommentGUID = iCommentGUID;
        Timestamp = Long.parseLong(iTimestamp);
    }
    public CommentModel(){};
}
