package io.brennanmcdonald.nonameapp.Helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.UUID;

import io.brennanmcdonald.nonameapp.Adapters.PictureListAdapter;
import io.brennanmcdonald.nonameapp.Models.CommentModel;
import io.brennanmcdonald.nonameapp.Models.PictureModel;
import io.brennanmcdonald.nonameapp.Models.UploadModel;

/**
 * Created by Brennan on 7/6/2015.
 */
public class AsyncHelper {
    private static final String DOMAIN = "http://ec2-54-148-117-166.us-west-2.compute.amazonaws.com/";

    /**
     * Created by Brennan on 6/27/2015.
     */
    public static class RetrievePictures extends AsyncTask<String, Void, PictureModel[]> {

        private Exception exception;
        private Context mContext;
        private View mView;
        ListView listView;
        ArrayList<PictureModel> ImageBmpList;
        PictureListAdapter adapter;


        public RetrievePictures(Context c, View v) {
            mContext = c;
            mView = v;
        }

        protected PictureModel[] doInBackground(String... urls) {

            try {
                URL url = new URL(urls[0]);
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setURI(new URI(DOMAIN + "api/picture/"));
                HttpResponse response = client.execute(request);
                String _Response = convertStreamToString(response.getEntity().getContent());
                Log.d("Resp", _Response);
                JSONArray DataArray = new JSONArray(_Response);
                PictureModel[] outArray;
                outArray = new PictureModel[DataArray.length()];

                for (int i = 0; i < DataArray.length(); i++) {
                    outArray[i] = (new PictureModel(
                            DataArray.getJSONObject(i).getString("ID"),
                            DataArray.getJSONObject(i).getString("Name"),
                            DataArray.getJSONObject(i).getString("Filename"),
                            DataArray.getJSONObject(i).getString("Width"),
                            DataArray.getJSONObject(i).getString("Height"),
                            DataArray.getJSONObject(i).getString("UserGuid"),
                            DataArray.getJSONObject(i).getString("Description"))
                    );
                }
                Log.e("PicDataArray", DataArray.toString());
                return outArray;
            } catch (Exception e) {
                Log.e("error", e.getMessage());
                return null;
            }
        }

        protected void onPostExecute(Pair<String, Integer[]> json) {

        }
    }

    public static class getPicture extends AsyncTask<String, Void, Bitmap> {

        public getPicture() {

        }

        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                Bitmap downloadedBitmap = new AmazonHelper().Download(params[0]);
                return downloadedBitmap;
            } catch (Exception e) {
                Log.e("GetPicture", e.getMessage());
                return null;
            }
        }
    }

    public static class addPictureToDatabase extends AsyncTask<UploadModel, Void, Boolean> {
        AmazonHelper AWS_AMZN = new AmazonHelper();

        public addPictureToDatabase() {

        }

        @Override
        protected Boolean doInBackground(UploadModel... params) {
            Bitmap toUpload = params[0].image;

            AWS_AMZN.Upload(toUpload, params[0].context, params[0].filename);

            HttpPost oPostRequest = new HttpPost();
            HttpClient oClient = new DefaultHttpClient();

            try {
                oPostRequest.setURI(new URI(DOMAIN + params[0].URI().replace(" ", "%20")));
                HttpResponse resp = oClient.execute(oPostRequest);
                return true;
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), e.getMessage());
            }
            return true;
        }
    }

    public static class addUser extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            String phoneNumber = params[0];
            String GUID = params[1];
            HttpPost getRequest;
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;

            try {
                getRequest = new HttpPost(new URI(DOMAIN + "api/Users/?PhoneNumber=" + phoneNumber.replace("+", "") + "&GUID=" + GUID.replace(" ", "%20")));
                response = client.execute(getRequest);
                return "";
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), e.getMessage());
                String s = e.getMessage();
                return e.getMessage();
            }

        }
    }

    public static class addComment extends AsyncTask<CommentModel, Void, Boolean> {

        @Override
        protected Boolean doInBackground(CommentModel... params) {
            CommentModel inComment = params[0];

            HttpPost postRequest;
            HttpClient client = new DefaultHttpClient();
            HttpResponse response;

            try {
                postRequest = new HttpPost(
                        new URI(DOMAIN + "api/Comments/?Name=" + inComment.ParentName +
                                "&GUID=" + inComment.UserGUID +
                                "&Message=" + inComment.Text +
                                "&CommentGUID=" + UUID.randomUUID().toString() +
                                "&Timestamp=" + System.currentTimeMillis())
                );
                response = client.execute(postRequest);
                return true;
            } catch (Exception e) {
                Log.e(getClass().getSimpleName(), e.getMessage());
                return false;
            }
        }
    }

    public static class getComments extends AsyncTask<String, Void, ArrayList<CommentModel>> {

        @Override
        protected ArrayList<CommentModel> doInBackground(String... params) {
            String PictureName = params[0];
            String Route = "api/Comments/?Name=" + PictureName;

            try {

                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setURI(new URI(DOMAIN + Route));
                HttpResponse response = client.execute(request);
                String _Response = convertStreamToString(response.getEntity().getContent());

                JSONArray DataArray = new JSONArray(_Response);

                HttpResponse Resp;

                ArrayList<CommentModel> outArray = new ArrayList<CommentModel>();

                for (int i = 0; i < DataArray.length(); i++) {
                    outArray.add(new CommentModel(
                                    DataArray.getJSONObject(i).getString("ID"),
                                    DataArray.getJSONObject(i).getString("ParentName"),
                                    DataArray.getJSONObject(i).getString("Text"),
                                    DataArray.getJSONObject(i).getString("UserGUID"),
                                    DataArray.getJSONObject(i).getString("CommentGUID"),
                                    DataArray.getJSONObject(i).getString("Timestamp"))
                    );
                }
                return outArray;

            } catch (Exception e) {
                Log.e("error", e.getMessage());
                return null;
            }
        }
    }


    public static String convertStreamToString(InputStream inputStream) throws IOException {
        Writer writer = new StringWriter();

        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 1024);
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (Exception e) {
            Log.e("ConvToStr", e.getMessage());
        } finally {
            inputStream.close();
        }
        return writer.toString();
    }

}
