package io.brennanmcdonald.nonameapp.Helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by Brennan on 7/6/2015.
 */
public class AmazonHelper {
    final static public String BUCKETNAME = "picture-api-bucket";
    final private String ACCESSKEY = "AKIAIIHCEMZRRJFNQLNA";
    final private String SECRETKEY = "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";

    final private AWSCredentials oCredProvider = new AWSCredentials() {
        @Override
        public String getAWSAccessKeyId() {
            return ACCESSKEY;
        }

        @Override
        public String getAWSSecretKey() {
            return SECRETKEY;
        }
    };

    public Boolean Upload(Bitmap b, Context c, String FileName)
    {
        File imageFile;
        TransferManager tm;
        try {
            File filesDir = c.getFilesDir();
            imageFile = new File(filesDir, FileName + ".png");

            tm = new TransferManager(oCredProvider);
            OutputStream os;


            os = new FileOutputStream(imageFile);
            Bitmap squareBitmap;
            if (b.getWidth() >= b.getHeight()){

                squareBitmap = Bitmap.createBitmap(
                        b,
                        b.getWidth()/2 - b.getHeight()/2,
                        0,
                        b.getHeight(),
                        b.getHeight()
                );

            }else{

                squareBitmap = Bitmap.createBitmap(
                        b,
                        0,
                        b.getHeight()/2 - b.getWidth()/2,
                        b.getWidth(),
                        b.getWidth()
                );
            }

            b.compress(Bitmap.CompressFormat.JPEG, 25, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
            return false;
        }

        try {
            tm.upload(BUCKETNAME, FileName, imageFile);
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
            return false;
        }
        return true;

    }

    public Bitmap Download(String Filename)
    {
        File inFile = new File(Filename);
        TransferManager tm;
        Bitmap b;
        try {
            tm = new TransferManager(oCredProvider);
            tm.download(BUCKETNAME, Filename, inFile);
            b = BitmapFactory.decodeFile(Filename);
            Log.e(getClass().getSimpleName(), "asdf");
        } catch(Exception e) {
            Log.e(getClass().getSimpleName(), e.getMessage());
            return null;
        }
        return b;
    }
}
